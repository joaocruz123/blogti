<?php

use App\Models\UserController;
use App\Repositories\UserControllerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserControllerRepositoryTest extends TestCase
{
    use MakeUserControllerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var UserControllerRepository
     */
    protected $userControllerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->userControllerRepo = App::make(UserControllerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateUserController()
    {
        $userController = $this->fakeUserControllerData();
        $createdUserController = $this->userControllerRepo->create($userController);
        $createdUserController = $createdUserController->toArray();
        $this->assertArrayHasKey('id', $createdUserController);
        $this->assertNotNull($createdUserController['id'], 'Created UserController must have id specified');
        $this->assertNotNull(UserController::find($createdUserController['id']), 'UserController with given id must be in DB');
        $this->assertModelData($userController, $createdUserController);
    }

    /**
     * @test read
     */
    public function testReadUserController()
    {
        $userController = $this->makeUserController();
        $dbUserController = $this->userControllerRepo->find($userController->id);
        $dbUserController = $dbUserController->toArray();
        $this->assertModelData($userController->toArray(), $dbUserController);
    }

    /**
     * @test update
     */
    public function testUpdateUserController()
    {
        $userController = $this->makeUserController();
        $fakeUserController = $this->fakeUserControllerData();
        $updatedUserController = $this->userControllerRepo->update($fakeUserController, $userController->id);
        $this->assertModelData($fakeUserController, $updatedUserController->toArray());
        $dbUserController = $this->userControllerRepo->find($userController->id);
        $this->assertModelData($fakeUserController, $dbUserController->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteUserController()
    {
        $userController = $this->makeUserController();
        $resp = $this->userControllerRepo->delete($userController->id);
        $this->assertTrue($resp);
        $this->assertNull(UserController::find($userController->id), 'UserController should not exist in DB');
    }
}
