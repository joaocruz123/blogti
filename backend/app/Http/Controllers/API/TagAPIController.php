<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTagAPIRequest;
use App\Http\Requests\API\UpdateTagAPIRequest;
use App\Models\Tag;
use App\Repositories\TagRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TagController
 * @package App\Http\Controllers\API
 */

class TagAPIController extends AppBaseController
{
    /** @var  TagRepository */
    private $tagRepository;

    public function __construct(TagRepository $tagRepo)
    {
        $this->tagRepository = $tagRepo;
    }

    /**
     * Display a listing of the Tag.
     * GET|HEAD /tags
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tags = Tag::all();
        return response()->json($tags);
    }

    /**
     * Store a newly created Tag in storage.
     * POST /tags
     *
     * @param CreateTagAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTagAPIRequest $request)
    {
        $data = $request->all();

        $validate = validator($data, $this->tagRepository->rules());
        if ( $validate->fails() ) {
            $messages = $validate->messages();
            return response()->json(['validate_error' => $messages], 422);
        }
        if (!$insert = $this->tagRepository->create($data) ) {
            return response()->json(['error' => 'error_insert'], 500);
        }

        return response()->json($insert , 201);
    }

    /**
     * Display the specified Tag.
     * GET|HEAD /tags/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tag = Tag::find($id);

        if(!$tag) {
            return response()->json([
                'message'   => 'Record not found',
            ], 404);
        }

        return response()->json($tag);
    }

    /**
     * Update the specified Tag in storage.
     * PUT/PATCH /tags/{id}
     *
     * @param  int $id
     * @param UpdateTagAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTagAPIRequest $request)
    {
        $data = $request->all();

        $validate = validator($data, $this->tagRepository->rules($id));
        if ( $validate->fails() ) {
            $messages = $validate->messages();
            return response()->json(['validate_error' => $messages], 422);
        }
        if( !$tag = $this->tagRepository->find($id)) {
            return response()->json(['error' => 'artigo_not_found'], 404);
        }
        if ( !$update = $tag->update($data) ) {
            return response()->json(['error' => 'artigo_not_update', 500]);
        }
        return response()->json($update, 200);
    }

    /**
     * Remove the specified Tag from storage.
     * DELETE /tags/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        if( !$tag = $this->tagRepository->find($id)) {
            return response()->json(['error' => 'artigo_not_found'], 404);
        }
        if ( !$delete = $tag->delete() ) {
            return response()->json(['error' => 'artigo_not_delete', 500]);
        }
        return response()->json($delete);
    }
}
