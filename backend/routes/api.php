<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*Route::post('login', 'UserAPIController@login');
Route::post('register', 'UserAPIController@register');
Route::group(['middleware' => 'auth:api'], function(){
Route::post('details', 'UserAPIController@details');
});*/

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');

});

Route::resource('artigos', 'ArtigoAPIController');
Route::resource('categorias', 'CategoriaAPIController');
Route::resource('comentarios', 'ComentarioAPIController');
Route::resource('tags', 'TagAPIController');
