## Sobre o Projeto
Este projeto é tem a finalidade de qualificar um integrante do time de desenvolvimento da AGV, como um Desenvolvedor WEB PHP FULL-STACK. Ele é um simple blog que possui uma aréa destinada  para o usuario postar artigos, gerenciar as categorias e os seus comentários.


## Requisitos do Projeto

- Laravel 5.6
- PHP >= 7.2.12
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension
- MariaDB


## Instalação
```
git clone https://gitlab.com/joaocruz123/blogti.git blog
cd blog
composer install
cp .env.example .env
php artisan key:generate
php artisan migrate
php artisan db:seed
```
`Acessar o sistema`

- Usuário: admin@admin.com
- Senha: 123456


## License
The Laravel framework is open-sourced software licensed under the MIT license.