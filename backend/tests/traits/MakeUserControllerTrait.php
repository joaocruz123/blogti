<?php

use Faker\Factory as Faker;
use App\Models\UserController;
use App\Repositories\UserControllerRepository;

trait MakeUserControllerTrait
{
    /**
     * Create fake instance of UserController and save it in database
     *
     * @param array $userControllerFields
     * @return UserController
     */
    public function makeUserController($userControllerFields = [])
    {
        /** @var UserControllerRepository $userControllerRepo */
        $userControllerRepo = App::make(UserControllerRepository::class);
        $theme = $this->fakeUserControllerData($userControllerFields);
        return $userControllerRepo->create($theme);
    }

    /**
     * Get fake instance of UserController
     *
     * @param array $userControllerFields
     * @return UserController
     */
    public function fakeUserController($userControllerFields = [])
    {
        return new UserController($this->fakeUserControllerData($userControllerFields));
    }

    /**
     * Get fake data of UserController
     *
     * @param array $postFields
     * @return array
     */
    public function fakeUserControllerData($userControllerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $userControllerFields);
    }
}
