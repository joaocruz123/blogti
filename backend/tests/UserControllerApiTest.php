<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserControllerApiTest extends TestCase
{
    use MakeUserControllerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateUserController()
    {
        $userController = $this->fakeUserControllerData();
        $this->json('POST', '/api/v1/userControllers', $userController);

        $this->assertApiResponse($userController);
    }

    /**
     * @test
     */
    public function testReadUserController()
    {
        $userController = $this->makeUserController();
        $this->json('GET', '/api/v1/userControllers/'.$userController->id);

        $this->assertApiResponse($userController->toArray());
    }

    /**
     * @test
     */
    public function testUpdateUserController()
    {
        $userController = $this->makeUserController();
        $editedUserController = $this->fakeUserControllerData();

        $this->json('PUT', '/api/v1/userControllers/'.$userController->id, $editedUserController);

        $this->assertApiResponse($editedUserController);
    }

    /**
     * @test
     */
    public function testDeleteUserController()
    {
        $userController = $this->makeUserController();
        $this->json('DELETE', '/api/v1/userControllers/'.$userController->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/userControllers/'.$userController->id);

        $this->assertResponseStatus(404);
    }
}
