<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArtigoTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artigo_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('artigo_id')->unsigned();
            $table->integer('tag_id')->unsigned();
            $table->foreign('artigo_id')->references('id')->on('artigos');
            $table->foreign('tag_id')->references('id')->on('tags');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artigo_tag');
    }
}
