export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer',
      badge: {
        variant: 'primary',
        text: 'NEW'
      }
    },
    {
      title: true,
      name: 'Blog',
      class: '',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Artigos',
      url: '/blog/artigos',
      icon: 'icon-docs'
    },
    {
      name: 'Categorias',
      url: '/blog/categorias',
      icon: 'icon-note'
    }
  ]
}
