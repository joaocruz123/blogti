<?php

namespace App\Repositories;

use App\Models\UserController;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class UserControllerRepository
 * @package App\Repositories
 * @version February 15, 2019, 11:24 pm UTC
 *
 * @method UserController findWithoutFail($id, $columns = ['*'])
 * @method UserController find($id, $columns = ['*'])
 * @method UserController first($columns = ['*'])
*/
class UserControllerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return UserController::class;
    }
}
